//
//  AIRatingTableViewCell.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 27.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AIRatingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISwitch *switcher;


@end
