//
//  AILocalDataManager.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AILocalDataManager.h"

@implementation AILocalDataManager

- (NSArray *)getCityData {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"cities" ofType:@"json"];
    
    NSError *error;
    
    NSString *fileContents = [NSString stringWithContentsOfFile:filePath
                                                       encoding:NSUTF8StringEncoding
                                                          error:&error];
    
    if (error)
        NSLog(@"Error reading file: %@",error.localizedDescription);
    
    return (NSArray *)[NSJSONSerialization
                       JSONObjectWithData:[fileContents dataUsingEncoding:NSUTF8StringEncoding]
                       options:0 error:NULL] ;
}

@end
