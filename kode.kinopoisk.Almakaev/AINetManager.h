//
//  AINetManager.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AINetManager : NSObject

+ (AINetManager *)sharedManager;

- (void)getData:(NSString *)url completition:(void (^)(id data, NSError *error))completitionBlock;

@end
