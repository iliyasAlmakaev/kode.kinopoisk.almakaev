//
//  AIUserDefaultsManager.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIUserDefaultsManager.h"

@interface AIUserDefaultsManager ()

@property (strong, nonatomic) NSUserDefaults *userDefaults;

@end

@implementation AIUserDefaultsManager

- (id)init
{
    self = [super init];
    if (self) {
        self.userDefaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

#pragma mark - Settings

- (void)setDefaultsSettings {
    if (![self.userDefaults objectForKey:@"cityName"]) {
        [self.userDefaults setObject:@"490" forKey:@"cityID"];
        [self.userDefaults setObject:@"Калининград" forKey:@"cityName"];
    }
    
    if (![self.userDefaults objectForKey:@"genreName"])
        [self.userDefaults setObject:@"любой" forKey:@"genreName"];

    [self.userDefaults synchronize];
}

- (void)setCityID:(NSString *)cityID setCityName:(NSString *)cityName {
    [self.userDefaults setObject:cityID forKey:@"cityID"];
    [self.userDefaults setObject:cityName forKey:@"cityName"];
    [self.userDefaults synchronize];
}

- (NSString *)getCityID {
   return [self.userDefaults objectForKey:@"cityID"];
}

- (NSString *)getCityName {
    return [self.userDefaults objectForKey:@"cityName"];
}

- (void)setGenreID:(NSString *)genreID setGenreName:(NSString *)genreName {
    [self.userDefaults setObject:genreID forKey:@"genreID"];
    [self.userDefaults setObject:genreName forKey:@"genreName"];
    [self.userDefaults synchronize];
}

- (NSString *)getGenreID {
    return [self.userDefaults objectForKey:@"genreID"];
}

- (NSString *)getGenreName {
    return [self.userDefaults objectForKey:@"genreName"];
}

- (void)setSortingRating:(BOOL)sortingRating {
    [self.userDefaults setBool:sortingRating forKey:@"sortingRating"];
    [self.userDefaults synchronize];
}

- (BOOL)getSortingRating {
    return [self.userDefaults boolForKey:@"sortingRating"];
}

#pragma mark - Film information

- (void)setFilmID:(NSString *)filmID {
    [self.userDefaults setObject:filmID forKey:@"filmID"];
    [self.userDefaults synchronize];
}

- (NSString *)getFilmID {
    return [self.userDefaults objectForKey:@"filmID"];
}

#pragma mark - Cities

- (void)setCityData:(NSArray *)cityData {
    [self.userDefaults setObject:cityData forKey:@"cityData"];
    [self.userDefaults synchronize];
}
- (NSArray *)getCityData {
    return [self.userDefaults objectForKey:@"cityData"];
}

#pragma mark - Dates for seance

- (void)setDatesForSeance:(NSString *)datesForSeance {
    [self.userDefaults setObject:datesForSeance forKey:@"datesForSeance"];
    [self.userDefaults synchronize];
}

- (NSString *)getDatesForSeance {
    return [self.userDefaults objectForKey:@"datesForSeance"];
}

#pragma mark - Cinema location

- (void)setCinemaName:(NSString *)cinemaName setLon:(NSString *)lon setLat:(NSString *)lat {
    [self.userDefaults setObject:cinemaName forKey:@"cinemaName"];
    [self.userDefaults setObject:lon forKey:@"lon"];
    [self.userDefaults setObject:lat forKey:@"lat"];
    [self.userDefaults synchronize];
}

- (NSString *)getCinemaName {
    return [self.userDefaults objectForKey:@"cinemaName"];
}

- (NSString *)getLon {
    return [self.userDefaults objectForKey:@"lon"];
}

- (NSString *)getLat {
    return [self.userDefaults objectForKey:@"lat"];
}

@end
