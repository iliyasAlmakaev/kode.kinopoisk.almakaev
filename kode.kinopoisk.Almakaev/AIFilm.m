//
//  AIFilm.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIFilm.h"

@implementation AIFilm

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{ @"name"       : @"name",
              @"filmID"     : @"filmID",
              @"imageURL"   : @"imageURL",
              @"rating"     : @"rating" };
}

@end
