//
//  AISettingsViewModel.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 15.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIBaseViewModel.h"
#import <ReactiveObjC.h>


@interface AISettingsViewModel : AIBaseViewModel

@property (readonly, nonatomic) NSString *city;
@property (readonly, nonatomic) NSString *currentCity;
@property (readonly, nonatomic) NSString *ganre;
@property (readonly, nonatomic) NSString *currentGanre;
@property (readonly, nonatomic) BOOL sortRating;
@property (readonly, nonatomic) RACCommand *back;
@property (readonly, nonatomic) RACCommand *openCities;
@property (readonly, nonatomic) RACCommand *openGanre;

- (void)viewDidLoad;

@end
