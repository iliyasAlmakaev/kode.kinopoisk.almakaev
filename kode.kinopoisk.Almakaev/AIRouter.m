//
//  AIRouter.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 11.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIRouter.h"
#import "AIBaseViewModel.h"
#import "AIFilmsViewModel.h"
#import "AIFilmInformationViewModel.h"
#import "AIFilmInformationViewController.h"
#import "AISettingsViewModel.h"
#import "AISettingsTableViewController.h"
#import "AIDatesForSeanceViewModel.h"
#import "AIDatesForSeanceTableViewController.h"
#import "AITimesForSeanceViewModel.h"
#import "AITimesForSeanceTableViewController.h"
#import "AICinemaLocationViewModel.h"
#import "AICinemaLocationViewController.h"
#import "AICitiesViewModel.h"
#import "AICitiesTableViewController.h"
#import "AIGangreViewModel.h"
#import "AIGanresTableViewController.h"

NSString *const error = @"Ошибка загрузки";

@interface AIRouter ()

@property (strong, nonatomic) AIFilmsTableViewController *filmsTableViewController;
@property (strong, nonatomic) AIFilmInformationViewController *filmInformationViewController;
@property (strong, nonatomic) AIDatesForSeanceTableViewController *datesForSeanceTableViewController;
@property (strong, nonatomic) AITimesForSeanceTableViewController *timesForSeanceTableViewController;
@property (strong, nonatomic) AICinemaLocationViewController *cinemaLocationViewController;
@property (strong, nonatomic) AISettingsTableViewController *settingsTableViewController;
@property (strong, nonatomic) AICitiesTableViewController *citiesTableViewController;
@property (strong, nonatomic) AIGanresTableViewController *ganresTableViewController;

@end

@implementation AIRouter

#pragma mark - Initializaiton

+ (AIRouter *)sharedManager {
    static AIRouter *router = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        router = [[AIRouter alloc] init];
    });
    
    return router;
}

- (instancetype)initWithTableViewController:(AIFilmsTableViewController *)filmsTableViewController;
{
    self = [super init];
    if (self) {
        self.filmsTableViewController = filmsTableViewController;
    }
    return self;
}

#pragma mark - Public methods

- (void)openFilmInformationViewControllerWithContext:(id)context {
    AIFilmInformationViewModel *filmInformationViewModel = [[AIFilmInformationViewModel alloc] init];
    filmInformationViewModel.context = context;
    self.filmInformationViewController =
    [[AIFilmInformationViewController alloc] initWithViewModel:filmInformationViewModel];
    
    [self nextViewController:self.filmInformationViewController currentViewController:self.filmsTableViewController];
}

- (void)closeFilmInformationViewController {
    [self closeCurrentViewController:self.filmInformationViewController];
}

- (void)openSettinsTableViewController {
    AISettingsViewModel *settingsViewModel = [[AISettingsViewModel alloc] init];
    self.settingsTableViewController =
    [[AISettingsTableViewController alloc] initWithViewModel:settingsViewModel];
    
    [self nextViewController:self.settingsTableViewController currentViewController:self.filmsTableViewController];
}

- (void)closeSettinsTableViewController {
    [self closeCurrentViewController:self.settingsTableViewController];
}

- (void)openCitiesTableViewController {
    AICitiesViewModel *citiesViewModel = [[AICitiesViewModel alloc] init];
    self.citiesTableViewController =
    [[AICitiesTableViewController alloc] initWithViewModel:citiesViewModel];
    
    [self nextViewController:self.citiesTableViewController currentViewController:self.settingsTableViewController];
}

- (void)closeCitiesTableViewController {
    [self closeCurrentViewController:self.citiesTableViewController];
}

- (void)openFilmsTableViewControllerFromCities {
    [self nextViewController:self.filmsTableViewController
       currentViewController:self.citiesTableViewController];
}

- (void)openGanreTableViewController {
    AIGangreViewModel *gangreViewModel = [[AIGangreViewModel alloc] init];
    self.ganresTableViewController =
    [[AIGanresTableViewController alloc] initWithViewModel:gangreViewModel];
    
    [self nextViewController:self.ganresTableViewController currentViewController:self.settingsTableViewController];
}

- (void)closeGanreTableViewController {
    [self closeCurrentViewController:self.ganresTableViewController];
}

- (void)openFilmsTableViewControllerFromGanre {
    [self nextViewController:self.filmsTableViewController
       currentViewController:self.ganresTableViewController];
}

- (void)openDatesForSeanceTableViewControllerWithContext:(id)context {
    AIDatesForSeanceViewModel *datesForSeanceViewModel = [[AIDatesForSeanceViewModel alloc] init];
    datesForSeanceViewModel.context = context;
    self.datesForSeanceTableViewController =
    [[AIDatesForSeanceTableViewController alloc] initWithViewModel:datesForSeanceViewModel];
    
    [self nextViewController:self.datesForSeanceTableViewController currentViewController:self.filmInformationViewController];
}

- (void)closeDatesForSeanceTableViewController {
    [self closeCurrentViewController:self.datesForSeanceTableViewController];
}

- (void)openTimesForSeanceTableViewControllerWithContext:(id)context {
    AITimesForSeanceViewModel *timesForSeanceViewModel = [[AITimesForSeanceViewModel alloc] init];
    timesForSeanceViewModel.context = context;
    self.timesForSeanceTableViewController =
    [[AITimesForSeanceTableViewController alloc] initWithViewModel:timesForSeanceViewModel];
    
    [self nextViewController:self.timesForSeanceTableViewController currentViewController:self.datesForSeanceTableViewController];
}

- (void)closeTimesForSeanceTableViewController {
    [self closeCurrentViewController:self.timesForSeanceTableViewController];
}

- (void)openCinemaLocationViewControllerWithContext:(id)context {
    AICinemaLocationViewModel *cinemaLocationViewModel = [[AICinemaLocationViewModel alloc] init];
    cinemaLocationViewModel.context = context;
    self.cinemaLocationViewController =
    [[AICinemaLocationViewController alloc] initWithViewModel:cinemaLocationViewModel];
    
    [self nextViewController:self.cinemaLocationViewController currentViewController:self.timesForSeanceTableViewController];
}

- (void)closeCinemaLocationViewController {
    [self closeCurrentViewController:self.cinemaLocationViewController];
}

- (void)showFilmsAlertViewError {
    [self alertView_currentViewController:self.filmsTableViewController];
}

- (void)showFilmInformationAlertViewError {
    [self alertView_currentViewController:self.filmInformationViewController];
}

- (void)showDatesForSeanceInformationAlertViewError {
    [self alertView_currentViewController:self.datesForSeanceTableViewController];
}

- (void)showTimesForSeanceAlertViewError {
    [self alertView_currentViewController:self.timesForSeanceTableViewController];
}

- (void)showGanreAlertViewError {
    [self alertView_currentViewController:self.ganresTableViewController];
}

#pragma mark - Private methods

- (void)nextViewController:(UIViewController *)nextViewController currentViewController:(UIViewController *)currentViewController {
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:nextViewController];
    
    navigationController.navigationBar.translucent = NO;
    navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    [currentViewController presentViewController:navigationController animated:YES completion:nil];
}

- (void)closeCurrentViewController:(UIViewController *)currentViewController {
    [currentViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)alertView_currentViewController:(UIViewController *)currentViewControler {
    UIAlertController * alert =  [UIAlertController alertControllerWithTitle:nil
                                                                     message:error
                                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    [currentViewControler presentViewController:alert animated:YES completion:nil];
}

@end
