//
//  AIFilmInformationViewModel.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 13.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIFilmInformationViewModel.h"
#import "AINetManager.h"
#import "AIUserDefaultsManager.h"
#import "AIFilmInformation.h"
#import "AIRouter.h"

@interface AIFilmInformationViewModel ()

@property (readonly, nonatomic) NSString *linkHD;
@property (readonly, nonatomic) NSString *linkSD;
@property (readonly, nonatomic) NSString *linkLow;

@end

@implementation AIFilmInformationViewModel

#pragma mark - Life cycle

- (void)viewDidLoad {
    [self getFilmInformation];
}

#pragma mark - Methods

- (void)getFilmInformation {
    NSString *url = @"getFilm?filmID=";
    url = [NSString stringWithFormat:@"%@%@", url, self.context];
    NSLog(@"context %@", self.context);

    _loadFilm = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {

        [[AINetManager sharedManager] getData:url completition:^(id data, NSError *error) {
            if (data) {
                AIFilmInformation *filmInformation = [[AIFilmInformation alloc] initWithData:data];
                
                _name = filmInformation.name;
                _slogan = filmInformation.slogan;
                _genre = filmInformation.genre;
                _country = filmInformation.country;
                _length = filmInformation.length;
                _rating = filmInformation.rating;
                _ageLimits = filmInformation.ageLimits;
                _linkHD = filmInformation.linkHD;
                _linkSD = filmInformation.linkSD;
                _linkLow = filmInformation.linkLow;
                _imageURL = filmInformation.imageURL;
                
                [subscriber sendNext:nil];
                [subscriber sendCompleted];
            } else {
                [subscriber sendError:error];
                [[AIRouter sharedManager] showFilmInformationAlertViewError];
            }
        }];
        return nil;
    }];
}

- (void)openURL:(NSString *)url {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (RACCommand *)openLinkHD {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [self openURL:self.linkHD];
                
                return [RACSignal empty];
            }];
}

- (RACCommand *)openLinkSD {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [self openURL:self.linkSD];
                
                return [RACSignal empty];
            }];
}

- (RACCommand *)openLinkLow {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [self openURL:self.linkLow];
                
                return [RACSignal empty];
            }];
}

- (RACCommand *)back {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] closeFilmInformationViewController];
                
                return [RACSignal empty];
            }];
}

- (RACCommand *)openDatesForSeance {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] openDatesForSeanceTableViewControllerWithContext:self.context];
                
                return [RACSignal empty];
            }];
}

@end
