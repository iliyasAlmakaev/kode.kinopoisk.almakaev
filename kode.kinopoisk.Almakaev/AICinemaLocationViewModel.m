//
//  AICinemaLocationViewModel.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 14.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AICinemaLocationViewModel.h"
#import "AIRouter.h"

static CGFloat const AICinemaLocationMetersPerMile = 1609.344f;

@implementation AICinemaLocationViewModel

- (void)viewDidLoad {
    _annotation = [[AIMapAnnotation alloc] init];
    
    [self setSinemaLocation];
}

#pragma mark - Methods

- (void)setSinemaLocation {
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [self.context[@"lat"] floatValue];
    zoomLocation.longitude= [self.context[@"lon"] floatValue];
    
    _viewRegion =
    MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*AICinemaLocationMetersPerMile, 0.5*AICinemaLocationMetersPerMile);
    
    _annotation.title = self.context[@"cinema"];
    _annotation.coordinate = zoomLocation;
}

- (RACCommand *)back {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] closeCinemaLocationViewController];
                
                return [RACSignal empty];
            }];
}

@end
