//
//  AIStringManager.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIStringManager.h"

NSString *const aiImageUrl = @"https://st.kp.yandex.net/images/";

@implementation AIStringManager

+ (NSString *)imageURL_string:(NSString *)string {
    NSString *sizeImageString = [string stringByReplacingOccurrencesOfString:@"iphone_" withString:@"iphone60_"];
    
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@", aiImageUrl, sizeImageString];
    
    return imageUrl;
}

+ (NSString *)imageURL_stringBig:(NSString *)string {
    NSString *sizeImageString = [string stringByReplacingOccurrencesOfString:@"iphone_" withString:@"iphone120_"];
    
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@", aiImageUrl, sizeImageString];
    
    return imageUrl;
}

+ (NSString *)ratingClear_string:(NSString *)string {
    if (string)
        string = [string substringToIndex:3];
    else
        string = @"не загружен";
    return string;
}

@end
