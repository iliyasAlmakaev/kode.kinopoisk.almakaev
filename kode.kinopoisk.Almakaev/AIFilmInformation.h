//
//  AIFilmInformation.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 29.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIFilmInformation : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *slogan;
@property (strong, nonatomic) NSString *genre;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *length;
@property (strong, nonatomic) NSString *rating;
@property (strong, nonatomic) NSString *ageLimits;
@property (strong, nonatomic) NSURL *imageURL;
@property (strong, nonatomic) NSString *linkHD;
@property (strong, nonatomic) NSString *linkSD;
@property (strong, nonatomic) NSString *linkLow;

- (id)initWithData:(id)data;

@end
