//
//  AIFilmInformation.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 29.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIFilmInformation.h"
#import <UIKit/UIKit.h>
#import "AIStringManager.h"

@implementation AIFilmInformation

- (id)initWithData:(id)data {
    self = [super init];
    if (self) {
        if (![data objectForKey:@"nameRU"]) {
            self.name = @"";
        } else {
        self.name = [data objectForKey:@"nameRU"];
        }
        if (![data objectForKey:@"slogan"]) {
            self.slogan = @"";
        } else {
        self.slogan = [data objectForKey:@"slogan"];
        }
        if (![data objectForKey:@"genre"]) {
            self.genre = @"";
        } else {
            self.genre = [data objectForKey:@"genre"];
        }
        if (![data objectForKey:@"country"]) {
            self.country = @"";
        } else {
            self.country = [data objectForKey:@"country"];
        }
        if (![data objectForKey:@"filmLength"]) {
            self.length = @"";
        } else {
            self.length = [data objectForKey:@"filmLength"];
        }
        if (![[data objectForKey:@"ratingData"] objectForKey:@"rating"] || ![[data objectForKey:@"ratingData"] objectForKey:@"ratingVoteCount"]) {
            self.rating = @"";
        } else {
            self.rating = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@ (%@)", [[data objectForKey:@"ratingData"] objectForKey:@"rating"], [[data objectForKey:@"ratingData"] objectForKey:@"ratingVoteCount"]]];
        }
        if (![data objectForKey:@"ratingAgeLimits"]) {
            self.ageLimits = @"";
        } else {
            self.ageLimits = [NSString stringWithFormat:@"%@+",[data objectForKey:@"ratingAgeLimits"]];
        }
        self.imageURL = [NSURL URLWithString:[AIStringManager imageURL_stringBig:[data objectForKey:@"posterURL"]]];
        if (![[data objectForKey:@"videoURL"] objectForKey:@"hd"]) {
            self.linkHD = @"";
        } else {
            self.linkHD = [[data objectForKey:@"videoURL"] objectForKey:@"hd"];
        }
        if (![[data objectForKey:@"videoURL"] objectForKey:@"sd"]) {
            self.linkSD = @"";
        } else {
            self.linkSD = [[data objectForKey:@"videoURL"] objectForKey:@"sd"];
        }
        if (![[data objectForKey:@"videoURL"] objectForKey:@"low"]) {
            self.linkLow = @"";
        } else {
            self.linkLow = [[data objectForKey:@"videoURL"] objectForKey:@"low"];
        }
        
    }
    return self;
}

@end

