//
//  AISettingsViewModel.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 15.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AISettingsViewModel.h"
#import "AIUserDefaultsManager.h"
#import "AIRouter.h"

@interface AISettingsViewModel ()

@property (strong, nonatomic) AIUserDefaultsManager *userDefaultsManager;

@end

@implementation AISettingsViewModel

- (void)viewDidLoad {
    self.userDefaultsManager = [[AIUserDefaultsManager alloc] init];
    
    _city = @"Город";
    _ganre = @"Жанр";
    _currentCity = [self.userDefaultsManager getCityName];
    _currentGanre = [self.userDefaultsManager getGenreName];
    _sortRating = [self.userDefaultsManager getSortingRating];
}

#pragma mark - Methods

- (RACCommand *)back {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] closeSettinsTableViewController];
                
                return [RACSignal empty];
            }];
}

- (RACCommand *)openCities {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] openCitiesTableViewController];
                
                return [RACSignal empty];
            }];
}

- (RACCommand *)openGanre {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] openGanreTableViewController];
                
                return [RACSignal empty];
            }];
}

@end
