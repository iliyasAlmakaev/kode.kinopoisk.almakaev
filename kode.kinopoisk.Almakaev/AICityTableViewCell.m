//
//  AICityTableViewCell.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 27.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AICityTableViewCell.h"

@implementation AICityTableViewCell

- (void)setModel:(id)model {
    id city = model;
    
    self.textLabel.text = city[@"name"];
}

@end
