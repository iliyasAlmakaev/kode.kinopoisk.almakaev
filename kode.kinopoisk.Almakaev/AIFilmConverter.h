//
//  FilmConverter.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface AIFilmConverter : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *filmId;
@property (strong, nonatomic) NSString *imageUrl;
@property (strong, nonatomic) NSString *rating;

@end
