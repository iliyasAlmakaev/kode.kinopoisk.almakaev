//
//  AICities.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AICities.h"

@implementation AICities

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{ @"cityID" : @"id",
              @"name"   : @"name" };
}

@end
