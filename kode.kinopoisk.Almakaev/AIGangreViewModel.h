//
//  AIGangreViewModel.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 16.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIBaseViewModel.h"
#import <ReactiveObjC.h>
#import "AIGenres.h"

@interface AIGangreViewModel : AIBaseViewModel

@property (readonly, nonatomic) NSArray <AIGenres *> *genres;
@property (strong, nonatomic) AIGenres *genre;
@property (readonly, nonatomic) RACSignal *loadGanres;
@property (readonly, nonatomic) RACCommand *back;
@property (readonly, nonatomic) RACCommand *didSelectItemCommand;

- (void)viewDidLoad;

@end
