//
//  AICinemaLocationViewModel.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 14.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIBaseViewModel.h"
#import <ReactiveObjC.h>
#import <MapKit/MapKit.h>
#import "AIMapAnnotation.h"

@interface AICinemaLocationViewModel : AIBaseViewModel

@property (readonly, nonatomic) AIMapAnnotation *annotation;
@property (readonly, nonatomic) MKCoordinateRegion viewRegion;
@property (readonly, nonatomic) RACCommand *back;

- (void)viewDidLoad;

@end
