//
//  AICinemaLocationViewController.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AICinemaLocationViewModel.h"

@interface AICinemaLocationViewController : UIViewController

- (instancetype)initWithViewModel:(AICinemaLocationViewModel *)viewModel;

@end
