//
//  FilmConverter.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIFilmConverter.h"

@implementation AIFilmConverter

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{ @"name"       : @"nameRU",
              @"filmId"     : @"id",
              @"imageUrl"   : @"posterURL",
              @"rating"     : @"rating" };
}

@end
