//
//  AIGenres.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 29.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIGenres.h"

@implementation AIGenres

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{ @"name"   : @"genreName",
              @"genreID" : @"genreID" };
}

@end
