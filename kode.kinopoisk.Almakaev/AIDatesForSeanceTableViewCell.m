//
//  AIDatesForSeanceTableViewCell.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIDatesForSeanceTableViewCell.h"

@implementation AIDatesForSeanceTableViewCell

- (void)setModel:(NSString *)model {
    self.textLabel.text = model;
}

@end
