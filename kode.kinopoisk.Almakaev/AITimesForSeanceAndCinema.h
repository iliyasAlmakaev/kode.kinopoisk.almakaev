//
//  AITimesForSeanceAndCinema.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 03.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface AITimesForSeanceAndCinema : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSArray *seance;
@property (strong, nonatomic) NSArray *seance3D;
@property (strong, nonatomic) NSString *cinema;
@property (strong, nonatomic) NSString *lon;
@property (strong, nonatomic) NSString *lat;

@end
