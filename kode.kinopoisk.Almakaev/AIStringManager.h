//
//  AIStringManager.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIStringManager : NSObject

+ (NSString *)imageURL_string:(NSString *)string;
+ (NSString *)imageURL_stringBig:(NSString *)string;
+ (NSString *)ratingClear_string:(NSString *)string;

@end
