//
//  AICitiesTableViewController.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 27.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AICitiesViewModel.h"

@interface AICitiesTableViewController : UITableViewController

- (instancetype)initWithViewModel:(AICitiesViewModel *)viewModel;

@end
