//
//  AITimesForSeanceTableViewController.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AITimesForSeanceTableViewController.h"
#import "AITimesForSeanceTableViewCell.h"
#import "AICinemaLocationTableViewCell.h"

static NSString *const TimesForSeanseCellID = @"timesForSeanseCellID";
static NSString *const CinemaLocationCellID = @"cinemaLocationCellID";
static NSString *const ErrorString = @"не загружено";

@interface AITimesForSeanceTableViewController ()

@property (strong, nonatomic) AITimesForSeanceViewModel *timesForSeanceViewModel;

@end

@implementation AITimesForSeanceTableViewController

- (instancetype)initWithViewModel:(AITimesForSeanceViewModel *)viewModel {
    self = [self init];
    if (self) {
        self.timesForSeanceViewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self.timesForSeanceViewModel viewDidLoad];
    [self bindUIWithViewModel];
}

#pragma mark - Methods

- (void)setupUI {
    self.navigationItem.title = @"Время показа";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"AITimesForSeanceTableViewCell" bundle:nil]
         forCellReuseIdentifier:TimesForSeanseCellID];
    [self.tableView registerNib:[UINib nibWithNibName:@"AICinemaLocationTableViewCell" bundle:nil]
         forCellReuseIdentifier:CinemaLocationCellID];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)bindUIWithViewModel {
    self.navigationItem.leftBarButtonItem.rac_command = self.timesForSeanceViewModel.back;
    
    [[self.timesForSeanceViewModel loadTimesForSeanceAndCinema]
     subscribeNext:^(id x) {
         [self.tableView reloadData];
     } error:^(NSError *error) {
         NSLog(@"An error occurred: %@", error);
     }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.timesForSeanceViewModel.cinema count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sectionContents = self.timesForSeanceViewModel.cinemaAndTime[section];
    
    NSInteger rows = [sectionContents count];
    
    if (![self.timesForSeanceViewModel.lon[section] isEqualToString:ErrorString] ||
        ![self.timesForSeanceViewModel.lat[section] isEqualToString:ErrorString]) {
        rows++;
    }
    return rows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.timesForSeanceViewModel.cinema[section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger counter = indexPath.section;
    
    NSArray *sectionContents = self.timesForSeanceViewModel.cinemaAndTime[counter];
    
    NSInteger rows = [sectionContents count];
    
    if ((indexPath.row == rows && ![self.timesForSeanceViewModel.lon[counter] isEqualToString:ErrorString]) ||
        (indexPath.row == rows && ![self.timesForSeanceViewModel.lat[counter] isEqualToString:ErrorString])) {
        AICinemaLocationTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:CinemaLocationCellID];
        
        return cell;
    } else {
        AITimesForSeanceTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:TimesForSeanseCellID];

        cell.textLabel.text = [sectionContents[indexPath.row] valueForKey:@"time"];
        
        return cell;
    }
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger counter = indexPath.section;
    
    NSArray *sectionContents = self.timesForSeanceViewModel.cinemaAndTime[counter];
    
    NSInteger rows = [sectionContents count];
    
    if (indexPath.row == rows) {
        NSDictionary *context = @{@"cinema" : self.timesForSeanceViewModel.cinema[counter],
                                  @"lon" : self.timesForSeanceViewModel.lon[counter],
                                  @"lat" : self.timesForSeanceViewModel.lat[counter]};
        
        [self.timesForSeanceViewModel.didSelectItemCommand
         execute:context];
    }
}

@end
