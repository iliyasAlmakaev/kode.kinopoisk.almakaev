//
//  FilmsTableViewController.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 27.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AIFilmsViewModel.h"


@interface AIFilmsTableViewController : UITableViewController

- (instancetype)initWithViewModel:(AIFilmsViewModel *)viewModel;

@end
