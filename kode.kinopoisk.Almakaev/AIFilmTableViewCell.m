//
//  AIFilmTableViewCell.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIFilmTableViewCell.h"
#import "AIFilmsViewModel.h"
#import "UIImageView+AFNetworking.h"

@interface AIFilmTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageFilm;
@property (weak, nonatomic) IBOutlet UILabel *nameFilm;
@property (weak, nonatomic) IBOutlet UILabel *ratingFilm;

@end

@implementation AIFilmTableViewCell

- (void)setModel:(AIFilm *)model {
    self.nameFilm.text = model.name;
    self.ratingFilm.text = [NSString stringWithFormat:@"Рейтинг: %@", model.rating];
    
    __weak typeof(self) nameSelf = self;
    
    [self.imageFilm setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.imageURL]] placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        nameSelf.imageFilm.image = image;
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        
    }];
}

@end
