//
//  AITimesForSeanceViewModel.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 14.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AITimesForSeanceViewModel.h"
#import "AIUserDefaultsManager.h"
#import "AINetManager.h"
#import "AITimesForSeanceAndCinema.h"
#import "AIRouter.h"

static NSString *const ErrorString = @"не загружено";

@interface AITimesForSeanceViewModel ()

@property (strong, nonatomic) AIUserDefaultsManager *userDefaultsManager;
@property (strong, nonatomic) AITimesForSeanceAndCinema *timeForSeanceAndCinema;
@property (strong, nonatomic) NSArray <AITimesForSeanceAndCinema *> *timesForSeanceAndCinema;

@end

@implementation AITimesForSeanceViewModel

- (void)viewDidLoad {
    self.userDefaultsManager = [[AIUserDefaultsManager alloc] init];
    
    _cinemaAndTime = [NSMutableArray array];
    _cinema = [NSMutableArray array];
    _lon = [NSMutableArray array];
    _lat = [NSMutableArray array];
}

#pragma mark - Methods

- (RACSignal *)loadTimesForSeanceAndCinema {
    NSString *url = [NSString stringWithFormat:@"getSeance?cityID=%@&filmID=%@&date=%@",
                     [self.userDefaultsManager getCityID], self.context[@"name"], self.context[@"date"]];
    
    @weakify(self)
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [[AINetManager sharedManager] getData:url completition:^(id data, NSError *error) {
            if ([data count]) {
                NSArray *timesForSeanceAndCinemaData = [data objectForKey:@"items"];
                
                self.timesForSeanceAndCinema =
                [MTLJSONAdapter modelsOfClass:[AITimesForSeanceAndCinema class]
                                fromJSONArray:timesForSeanceAndCinemaData
                                        error:nil];
                
                for (AITimesForSeanceAndCinema *timeAndCinema in self.timesForSeanceAndCinema) {
                    if (timeAndCinema.seance) {
                        [_cinemaAndTime addObject:timeAndCinema.seance];
                    } else if (!timeAndCinema.seance3D) {
                        [_cinemaAndTime addObject:ErrorString];
                    }
                    if (timeAndCinema.seance3D) {
                        [_cinemaAndTime addObject:timeAndCinema.seance3D];
                    } else if (!timeAndCinema.seance) {
                        [_cinemaAndTime addObject:ErrorString];
                    }
                    if (timeAndCinema.cinema) {
                        [_cinema addObject:timeAndCinema.cinema];
                    } else {
                        [_cinema addObject:ErrorString];
                    }
                    if (timeAndCinema.lon && timeAndCinema.lat) {
                        [_lon addObject:timeAndCinema.lon];
                        [_lat addObject:timeAndCinema.lat];
                    } else {
                        [_lon addObject:ErrorString];
                        [_lat addObject:ErrorString];
                    }
                }
                [subscriber sendNext:nil];
                [subscriber sendCompleted];
            } else if (error) {
                [subscriber sendError:error];
                [[AIRouter sharedManager] showTimesForSeanceAlertViewError];
            }
        }];
        return nil;
    }];
}

- (RACCommand *)didSelectItemCommand {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] openCinemaLocationViewControllerWithContext:input];
                
                return [RACSignal empty];
            }];
}

- (RACCommand *)back {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] closeTimesForSeanceTableViewController];
                
                return [RACSignal empty];
            }];
}

@end
