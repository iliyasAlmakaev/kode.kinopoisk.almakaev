//
//  AIFilmTableViewCell.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AIFilmTableViewCell : UITableViewCell

@property (nonatomic, strong) id model;

@end
