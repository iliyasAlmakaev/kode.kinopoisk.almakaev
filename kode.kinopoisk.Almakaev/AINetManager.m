//
//  AINetManager.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AINetManager.h"
#import "AFNetworking.h"

NSString *const aiApiUrl = @"https://api.kinopoisk.cf/";

@interface AINetManager ()

@property (strong, nonatomic) AFHTTPSessionManager *requestOperationManager;

@end

@implementation AINetManager

+ (AINetManager *)sharedManager {
    static AINetManager *manager = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[AINetManager alloc] init];
    });
    
    return manager;
}

- (id)init
{
    self = [super init];
    if (self) {
        NSURL *url = [NSURL URLWithString:aiApiUrl];
        
        self.requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    }
    return self;
}

- (void)getData:(NSString *)url completition:(void (^)(id, NSError *))completitionBlock {
    [self.requestOperationManager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        completitionBlock(responseObject, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        
        completitionBlock(nil, error);
    }];
}

@end
