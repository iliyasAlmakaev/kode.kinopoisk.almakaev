//
//  AIFilm.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface AIFilm : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *filmID;
@property (strong, nonatomic) NSString *imageURL;
@property (strong, nonatomic) NSString *rating;

@end
