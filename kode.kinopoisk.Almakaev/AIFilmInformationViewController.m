//
//  AIFilmInformationViewController.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 29.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIFilmInformationViewController.h"
#import "UIImageView+AFNetworking.h"

@interface AIFilmInformationViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *nameRu;
@property (weak, nonatomic) IBOutlet UILabel *slogan;
@property (weak, nonatomic) IBOutlet UILabel *genre;
@property (weak, nonatomic) IBOutlet UILabel *country;
@property (weak, nonatomic) IBOutlet UILabel *length;
@property (weak, nonatomic) IBOutlet UILabel *rating;
@property (weak, nonatomic) IBOutlet UILabel *ageLimits;

@property (weak, nonatomic) IBOutlet UIButton *buttonLinkHD;
@property (weak, nonatomic) IBOutlet UIButton *buttonLinkSD;
@property (weak, nonatomic) IBOutlet UIButton *buttonLinkLow;

@property (strong, nonatomic) AIFilmInformationViewModel *filmInformationViewModel;

@end

@implementation AIFilmInformationViewController

- (instancetype)initWithViewModel:(AIFilmInformationViewModel *)viewModel {
    self = [self init];
    if (self) {
        self.filmInformationViewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self.filmInformationViewModel viewDidLoad];
    [self bindUIWithViewModel];
}

#pragma mark - Methods

- (void)setupUI {
    self.navigationItem.title = @"Информация";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"date"]
                                                                              style:UIBarButtonItemStyleDone
                                                                             target:nil
                                                                             action:nil];
}

- (void)bindUIWithViewModel {
    [[self.filmInformationViewModel loadFilm]
     subscribeNext:^(id x) {
         self.nameRu.text = self.filmInformationViewModel.name;
         self.slogan.text = self.filmInformationViewModel.slogan;
         self.genre.text = self.filmInformationViewModel.genre;
         self.country.text = self.filmInformationViewModel.country;
         self.length.text = self.filmInformationViewModel.length;
         self.rating.text = self.filmInformationViewModel.rating;
         self.ageLimits.text = self.filmInformationViewModel.ageLimits;
         [self.image setImageWithURL:self.filmInformationViewModel.imageURL];
     } error:^(NSError *error) {
         NSLog(@"An error occurred: %@", error);
     }];
    
    if (!self.filmInformationViewModel.openLinkHD) {
        self.buttonLinkHD.enabled = false;
    } else {
        self.buttonLinkHD.rac_command = self.filmInformationViewModel.openLinkHD;
    }
    if (!self.filmInformationViewModel.openLinkSD) {
        self.buttonLinkSD.enabled = false;
    } else {
        self.buttonLinkSD.rac_command = self.filmInformationViewModel.openLinkSD;
    }
    if (!self.filmInformationViewModel.openLinkLow) {
        self.buttonLinkLow.enabled = false;
    } else {
        self.buttonLinkLow.rac_command = self.filmInformationViewModel.openLinkLow;
    }
    
    self.navigationItem.leftBarButtonItem.rac_command = self.filmInformationViewModel.back;
    self.navigationItem.rightBarButtonItem.rac_command = self.filmInformationViewModel.openDatesForSeance;
}

@end
