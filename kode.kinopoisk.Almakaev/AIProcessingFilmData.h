//
//  AIProcessingFilmData.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 29.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AIFilm.h"
#import "AIUserDefaultsManager.h"

@interface AIProcessingFilmData : NSObject

- (NSArray *)structuringFilmData:(id)data
              userDefaultManager:(AIUserDefaultsManager *)userDefaultsManager;

@end
