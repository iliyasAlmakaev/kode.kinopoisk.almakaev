//
//  AIRouter.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 11.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AIFilmsTableViewController.h"

@interface AIRouter : NSObject

+ (AIRouter *)sharedManager;
- (instancetype)initWithTableViewController:(AIFilmsTableViewController *)filmsTableViewController;
- (void)openFilmInformationViewControllerWithContext:(id)context;
- (void)closeFilmInformationViewController;
- (void)openSettinsTableViewController;
- (void)closeSettinsTableViewController;
- (void)openCitiesTableViewController;
- (void)closeCitiesTableViewController;
- (void)openFilmsTableViewControllerFromCities;
- (void)openGanreTableViewController;
- (void)closeGanreTableViewController;
- (void)openFilmsTableViewControllerFromGanre;
- (void)openDatesForSeanceTableViewControllerWithContext:(id)context;
- (void)closeDatesForSeanceTableViewController;
- (void)openTimesForSeanceTableViewControllerWithContext:(id)context;
- (void)closeTimesForSeanceTableViewController;
- (void)openCinemaLocationViewControllerWithContext:(id)context;
- (void)closeCinemaLocationViewController;
- (void)showFilmsAlertViewError;
- (void)showFilmInformationAlertViewError;
- (void)showDatesForSeanceInformationAlertViewError;
- (void)showTimesForSeanceAlertViewError;
- (void)showGanreAlertViewError;

@end
