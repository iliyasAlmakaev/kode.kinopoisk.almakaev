//
//  AITimesForSeanceTableViewController.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AITimesForSeanceViewModel.h"

@interface AITimesForSeanceTableViewController : UITableViewController

- (instancetype)initWithViewModel:(AITimesForSeanceViewModel *)viewModel;

@end
