//
//  AITimesForSeanceAndCinema.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 03.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AITimesForSeanceAndCinema.h"

@implementation AITimesForSeanceAndCinema

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{ @"seance" : @"seance",
              @"seance3D" : @"seance3D",
              @"cinema" : @"cinemaName",
              @"lon"    : @"lon",
              @"lat"    : @"lat" };
}

@end
