//
//  AIBaseViewModel.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 11.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIBaseViewModel : NSObject

@property (strong, nonatomic) id context;

@end
