//
//  AICitiesViewModel.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 16.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AICitiesViewModel.h"
#import "AIRouter.h"
#import "AIUserDefaultsManager.h"
#import "AILocalDataManager.h"

@interface AICitiesViewModel ()

@property (strong, nonatomic) AILocalDataManager *localDataManager;
@property (strong, nonatomic) AIUserDefaultsManager *userDefaultManager;

@end

@implementation AICitiesViewModel

- (void)viewDidLoad {
    self.localDataManager = [[AILocalDataManager alloc] init];
    self.userDefaultManager = [[AIUserDefaultsManager alloc] init];
    
    [self getCities];
}

#pragma mark - Methods

- (void)getCities {
    if (![self.userDefaultManager getCityData])
        [self.userDefaultManager setCityData:[self.localDataManager getCityData]];
    
    _cities = [self.userDefaultManager getCityData];
}

- (RACCommand *)back {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] closeCitiesTableViewController];
                
                return [RACSignal empty];
            }];
}

- (RACCommand *)didSelectItemCommand {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                id city = input;
                
                [self.userDefaultManager setCityID:city[@"id"] setCityName:city[@"name"]];
                
                [[AIRouter sharedManager] openFilmsTableViewControllerFromCities];
                
                return [RACSignal empty];
            }];
}

@end
