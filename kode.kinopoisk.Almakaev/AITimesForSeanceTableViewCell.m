//
//  AITimesForSeanceTableViewCell.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AITimesForSeanceTableViewCell.h"

@implementation AITimesForSeanceTableViewCell

- (void)setModel:(NSString *)model {
    self.textLabel.text = model;
}

@end
