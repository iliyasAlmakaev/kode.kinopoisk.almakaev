//
//  AITimesForSeanceViewModel.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 14.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIBaseViewModel.h"
#import <ReactiveObjC.h>

@interface AITimesForSeanceViewModel : AIBaseViewModel

@property (readonly, nonatomic) NSMutableArray *cinemaAndTime;
@property (readonly, nonatomic) NSMutableArray *cinema;
@property (readonly, nonatomic) NSMutableArray *lon;
@property (readonly, nonatomic) NSMutableArray *lat;
@property (readonly, nonatomic) RACSignal *loadTimesForSeanceAndCinema;
@property (readonly, nonatomic) RACCommand *didSelectItemCommand;
@property (readonly, nonatomic) RACCommand *back;

- (void)viewDidLoad;

@end
