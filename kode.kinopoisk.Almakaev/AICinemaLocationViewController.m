//
//  AICinemaLocationViewController.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AICinemaLocationViewController.h"
#import <MapKit/MapKit.h>

@interface AICinemaLocationViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) AICinemaLocationViewModel *cinemaLocationViewModel;

@end

@implementation AICinemaLocationViewController

- (instancetype)initWithViewModel:(AICinemaLocationViewModel *)viewModel {
    self = [self init];
    if (self) {
        self.cinemaLocationViewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self.cinemaLocationViewModel viewDidLoad];
    [self bindUIWithViewModel];
}

#pragma mark - Methods

- (void)setupUI {
    self.navigationItem.title = @"Местоположение";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)bindUIWithViewModel {
    self.navigationItem.leftBarButtonItem.rac_command = self.cinemaLocationViewModel.back;
    
    [self.mapView addAnnotation:self.cinemaLocationViewModel.annotation];
    [self.mapView setRegion:self.cinemaLocationViewModel.viewRegion animated:YES];
}


@end
