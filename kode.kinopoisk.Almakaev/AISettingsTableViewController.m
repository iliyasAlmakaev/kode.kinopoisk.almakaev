//
//  AISettingsTableViewController.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 27.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AISettingsTableViewController.h"
#import "AICityAndGenreTableViewCell.h"
#import "AIRatingTableViewCell.h"
#import "AIUserDefaultsManager.h"


static NSString *const CityAndGenreCellID = @"cityAndGenreCellID";
static NSString *const RatingCellID = @"ratingCellID";

@interface AISettingsTableViewController ()

@property (strong, nonatomic) AIUserDefaultsManager *userDefaultsManager;
@property (strong, nonatomic) AISettingsViewModel *settingsViewModel;

@end

@implementation AISettingsTableViewController

- (instancetype)initWithViewModel:(AISettingsViewModel *)viewModel {
    self = [self init];
    if (self) {
        self.settingsViewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.userDefaultsManager = [[AIUserDefaultsManager alloc] init];
    
    [self setupUI];    
    [self bindUIWithViewModel];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.settingsViewModel viewDidLoad];
    [self.tableView reloadData];
}

#pragma mark - Methods

- (void)setupUI {
    self.navigationItem.title = @"Настройки";

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"done"]
                                                                              style:UIBarButtonItemStyleDone
                                                                             target:nil
                                                                             action:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"AICityAndGenreTableViewCell" bundle:nil]
         forCellReuseIdentifier:CityAndGenreCellID];
    [self.tableView registerNib:[UINib nibWithNibName:@"AIRatingTableViewCell" bundle:nil]
         forCellReuseIdentifier:RatingCellID];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)bindUIWithViewModel {
    self.navigationItem.rightBarButtonItem.rac_command = self.settingsViewModel.back;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < 2) {
        AICityAndGenreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CityAndGenreCellID];
        
        if (indexPath.row == 0) {
            cell.name.text = self.settingsViewModel.city;
            cell.currentName.text = self.settingsViewModel.currentCity;
        } else {
            cell.name.text = self.settingsViewModel.ganre;
            cell.currentName.text = self.settingsViewModel.currentGanre;
        }
        return cell;
    } else {
        AIRatingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RatingCellID];
        
        [cell.switcher addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        
        if ([self.userDefaultsManager getSortingRating])
            [cell.switcher setOn:YES animated:NO];
            else
                [cell.switcher setOn:NO animated:NO];
        
        return cell;
    }
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [self.settingsViewModel.openCities execute:nil];
    } else if (indexPath.row == 1) {
        [self.settingsViewModel.openGanre execute:nil];
    }
}

#pragma mark - Switch

- (void)switchChanged:(id)sender {
    UISwitch* switchControl = sender;
    
    if (switchControl.on)
        [self.userDefaultsManager setSortingRating:YES];
    else
        [self.userDefaultsManager setSortingRating:NO];
}

@end
