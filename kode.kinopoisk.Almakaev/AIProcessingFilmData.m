//
//  AIProcessingFilmData.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 29.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIProcessingFilmData.h"
#import "AIFilmTableViewCell.h"
#import "AIFilmConverter.h"
#import "AIStringManager.h"


@implementation AIProcessingFilmData

- (NSArray *)structuringFilmData:(id)data
              userDefaultManager:(AIUserDefaultsManager *)userDefaultsManager {
    
    NSArray <AIFilmConverter *> *films = [MTLJSONAdapter modelsOfClass:[AIFilmConverter class] fromJSONArray:[data valueForKeyPath:@"filmsData"] error:nil];
    
    NSArray *dataFilms = [NSMutableArray array];
    NSMutableArray *notSortedFilmsByRating = [NSMutableArray array];
    NSArray *sortedFilmsByRating = [NSArray array];
    
    [self unionFilmData:notSortedFilmsByRating structuredFilmData:films];
    
    if ([userDefaultsManager getSortingRating]) {
        dataFilms = [self sortingFilmData:sortedFilmsByRating notSortedFilmData:notSortedFilmsByRating];
    } else {
        dataFilms = notSortedFilmsByRating;
    }
    
    return dataFilms;
}

- (void)unionFilmData:(NSMutableArray *)unionFilmData structuredFilmData:(NSArray <AIFilmConverter *> *)structuredFilmData {
    NSString *name = @"name";
    NSString *filmID = @"filmID";
    NSString *imageURL = @"imageURL";
    NSString *rating = @"rating";
    
    for (AIFilmConverter *film in structuredFilmData) {
        NSString *imageUrlComplite = [AIStringManager imageURL_string:film.imageUrl];
        NSString *ratingComplite = [AIStringManager ratingClear_string:film.rating];
        
        NSDictionary *dictionaryFilm = @{ name      : film.name,
                                          filmID    : film.filmId,
                                          imageURL  : imageUrlComplite,
                                          rating    : ratingComplite };
        
        [unionFilmData addObject:dictionaryFilm];
    }
  //  NSLog(@"get films complite %@", unionFilmData);
    
}

- (NSArray *)sortingFilmData:(NSArray *)sortedFilmData notSortedFilmData:(NSMutableArray *)notSortedFilmData {
   sortedFilmData = [notSortedFilmData sortedArrayUsingComparator: ^(id obj1, id obj2) {
        if ([[obj1 objectForKey:@"rating"] doubleValue] < [[obj2 objectForKey:@"rating"] doubleValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([[obj1 objectForKey:@"rating"] doubleValue] > [[obj2 objectForKey:@"rating"] doubleValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    
   // NSLog(@"get sorted films complite %@", sortedFilmData);
    return sortedFilmData;
}

@end
