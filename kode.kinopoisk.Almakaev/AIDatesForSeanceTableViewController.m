//
//  AIDatesForSeanceTableViewController.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIDatesForSeanceTableViewController.h"
#import "AIDatesForSeanceTableViewCell.h"

static NSString *const DatesForSeanceCellID = @"datesForSeanceCellID";

@interface AIDatesForSeanceTableViewController ()

@property (strong, nonatomic) AIDatesForSeanceViewModel *dateForSeanceViewModel;

@end

@implementation AIDatesForSeanceTableViewController

- (instancetype)initWithViewModel:(AIDatesForSeanceViewModel *)viewModel {
    self = [self init];
    if (self) {
        self.dateForSeanceViewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self.dateForSeanceViewModel viewDidLoad];
    [self bindUIWithViewModel];
}

#pragma mark - Methods

- (void)setupUI {
    self.navigationItem.title = @"Даты показа";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"AIDatesForSeanceTableViewCell" bundle:nil]
         forCellReuseIdentifier:DatesForSeanceCellID];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)bindUIWithViewModel {
    self.navigationItem.leftBarButtonItem.rac_command = self.dateForSeanceViewModel.back;
    
    [[self.dateForSeanceViewModel loadDatesForSeance]
     subscribeNext:^(id x) {
         [self.tableView reloadData];
     } error:^(NSError *error) {
         NSLog(@"An error occurred: %@", error);
     }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dateForSeanceViewModel.dates count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AIDatesForSeanceTableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:DatesForSeanceCellID];
    
    cell.model = self.dateForSeanceViewModel.dates[indexPath.row];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {   
    [self.dateForSeanceViewModel.didSelectItemCommand
     execute:self.dateForSeanceViewModel.dates[indexPath.row]];
}

@end
