//
//  AIFilmsViewModel.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 10.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AIFilm.h"
#import <ReactiveObjC.h>

@interface AIFilmsViewModel : NSObject

@property (readonly, nonatomic) NSArray <AIFilm *> *films;
@property (strong, nonatomic) AIFilm *film;
@property (readonly, nonatomic) RACSignal *loadFilms;
@property (readonly, nonatomic) RACCommand *didSelectItemCommand;
@property (readonly, nonatomic) RACCommand *openSettings;

- (void)viewDidLoad;

@end
