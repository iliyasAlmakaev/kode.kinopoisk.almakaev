//
//  AICitiesTableViewController.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 27.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AICitiesTableViewController.h"
#import "AICityTableViewCell.h"

static NSString *const CityCellID = @"cityCellID";

@interface AICitiesTableViewController ()

@property (strong, nonatomic) AICitiesViewModel *citiesViewModel;

@end

@implementation AICitiesTableViewController

- (instancetype)initWithViewModel:(AICitiesViewModel *)viewModel {
    self = [self init];
    if (self) {
        self.citiesViewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self.citiesViewModel viewDidLoad];
    [self bindUIWithViewModel];
}

#pragma mark - Methods

- (void)setupUI {
    self.navigationItem.title = @"Города";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"AICityTableViewCell" bundle:nil]
         forCellReuseIdentifier:CityCellID];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)bindUIWithViewModel {
    self.navigationItem.leftBarButtonItem.rac_command = self.citiesViewModel.back;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.citiesViewModel.cities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AICityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CityCellID];
    
    cell.model = self.citiesViewModel.cities[indexPath.row];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.citiesViewModel.didSelectItemCommand execute:self.citiesViewModel.cities[indexPath.row]];
}

@end
