//
//  AILocalDataManager.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AILocalDataManager : NSObject

- (NSArray *)getCityData;

@end
