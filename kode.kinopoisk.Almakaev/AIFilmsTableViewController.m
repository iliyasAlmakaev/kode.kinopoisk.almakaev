//
//  AIFilmsTableViewController.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 27.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIFilmsTableViewController.h"
#import "AIFilmTableViewCell.h"

static NSString *const FilmCellID = @"filmCellID";

@interface AIFilmsTableViewController ()

@property (weak, nonatomic) AIFilmsViewModel *filmsViewModel;

@end

@implementation AIFilmsTableViewController

- (instancetype)initWithViewModel:(AIFilmsViewModel *)viewModel {
    self = [self init];
    if (self) {
        self.filmsViewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self.filmsViewModel viewDidLoad];
    [self bindUIWithViewModel];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self updateFilms];
}

#pragma mark - Methods

- (void)setupUI {
    self.navigationItem.title = @"Фильмы";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"]
                                                                              style:UIBarButtonItemStyleDone
                                                                             target:nil
                                                                             action:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"AIFilmTableViewCell" bundle:nil]
         forCellReuseIdentifier:FilmCellID];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)bindUIWithViewModel {
    self.navigationItem.rightBarButtonItem.rac_command = self.filmsViewModel.openSettings;
}

- (void)updateFilms {
    [[self.filmsViewModel loadFilms]
     subscribeNext:^(id x) {
         [self.tableView reloadData];
     } error:^(NSError *error) {
         NSLog(@"An error occurred: %@", error);
     }];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 105;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.filmsViewModel.films count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AIFilmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FilmCellID];
    
    cell.model = self.filmsViewModel.films[indexPath.row];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.filmsViewModel.film = self.filmsViewModel.films[indexPath.row];
    
    [self.filmsViewModel.didSelectItemCommand execute:self.filmsViewModel.film.filmID];
}

@end
