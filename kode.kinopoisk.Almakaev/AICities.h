//
//  AICities.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 30.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface AICities : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *cityID;

@end
