//
//  AIFilmsViewModel.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 10.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIFilmsViewModel.h"
#import "AIUserDefaultsManager.h"
#import "AIProcessingFilmData.h"
#import "AINetManager.h"
#import "AIRouter.h"

@interface AIFilmsViewModel ()

@property (strong, nonatomic) AIProcessingFilmData *processingFilmData;
@property (strong, nonatomic) AIUserDefaultsManager *userDefaultsManager;

@end

@implementation AIFilmsViewModel

#pragma mark - Life cycle

- (void)viewDidLoad {
    self.processingFilmData = [[AIProcessingFilmData alloc] init];
    self.userDefaultsManager = [[AIUserDefaultsManager alloc] init];
    
    [self.userDefaultsManager setDefaultsSettings];
}

#pragma mark - Methods

- (RACSignal *)loadFilms {
    NSString *url = @"getTodayFilms";
    NSLog(@"id ganre %@", [self.userDefaultsManager getGenreID]);
    if ([self.userDefaultsManager getGenreID]) {
        url = [NSString stringWithFormat:@"%@?genreID=%@", url, [self.userDefaultsManager getGenreID]];
    }
    
    @weakify(self)
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [[AINetManager sharedManager] getData:url completition:^(id data, NSError *error) {
            if (data) {
                NSArray *dataFilms = [self.processingFilmData structuringFilmData:data
                                                               userDefaultManager:self.userDefaultsManager];
                
                _films = [MTLJSONAdapter modelsOfClass:[AIFilm class]
                                         fromJSONArray:dataFilms
                                                 error:nil];
                
                [subscriber sendNext:nil];
                [subscriber sendCompleted];
            } else {
                [subscriber sendError:error];
                [[AIRouter sharedManager] showFilmsAlertViewError];
            }
        }];
        return nil;
    }];
}

- (RACCommand *)didSelectItemCommand {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] openFilmInformationViewControllerWithContext:input];
                                 
                return [RACSignal empty];
            }];
}

- (RACCommand *)openSettings {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] openSettinsTableViewController];
                
                return [RACSignal empty];
            }];
}

@end
