//
//  AIFilmInformationViewModel.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 13.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIBaseViewModel.h"
#import <ReactiveObjC.h>

@interface AIFilmInformationViewModel : AIBaseViewModel

@property (readonly, nonatomic) NSString *name;
@property (readonly, nonatomic) NSString *slogan;
@property (readonly, nonatomic) NSString *genre;
@property (readonly, nonatomic) NSString *country;
@property (readonly, nonatomic) NSString *length;
@property (readonly, nonatomic) NSString *rating;
@property (readonly, nonatomic) NSString *ageLimits;
@property (readonly, nonatomic) NSURL *imageURL;
@property (readonly, nonatomic) RACSignal *loadFilm;
@property (readonly, nonatomic) RACCommand *openLinkHD;
@property (readonly, nonatomic) RACCommand *openLinkSD;
@property (readonly, nonatomic) RACCommand *openLinkLow;
@property (readonly, nonatomic) RACCommand *back;
@property (readonly, nonatomic) RACCommand *openDatesForSeance;

- (void)viewDidLoad;

@end
