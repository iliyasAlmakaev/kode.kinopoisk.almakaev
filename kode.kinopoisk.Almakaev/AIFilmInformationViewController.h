//
//  AIFilmInformationViewController.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 29.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AIFilmInformationViewModel.h"

@interface AIFilmInformationViewController : UIViewController

- (instancetype)initWithViewModel:(AIFilmInformationViewModel *)viewModel;

@end
