//
//  AIDatesForSeanceViewModel.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 14.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIDatesForSeanceViewModel.h"
#import "AIUserDefaultsManager.h"
#import "AINetManager.h"
#import "AIRouter.h"

@interface AIDatesForSeanceViewModel ()

@property (strong, nonatomic) AIUserDefaultsManager *userDefaultsManager;

@end

@implementation AIDatesForSeanceViewModel

#pragma mark - Life cycle

- (void)viewDidLoad {
    self.userDefaultsManager = [[AIUserDefaultsManager alloc] init];
}

#pragma mark - Methods

- (RACSignal *)loadDatesForSeance {
    NSString *url = [NSString stringWithFormat:@"getDatesForSeance?cityID=%@&filmID=%@",
                     [self.userDefaultsManager getCityID], self.context];

    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[AINetManager sharedManager] getData:url completition:^(id data, NSError *error) {
            if ([data count]) {
                _dates = [data objectForKey:@"dates"];
                
                [subscriber sendNext:nil];
                [subscriber sendCompleted];
            } else if (error) {
                [subscriber sendError:error];
                [[AIRouter sharedManager] showDatesForSeanceInformationAlertViewError];
            }
        }];
        return nil;
    }];
}

- (RACCommand *)back {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] closeDatesForSeanceTableViewController];
                
                return [RACSignal empty];
            }];
}

- (RACCommand *)didSelectItemCommand {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                NSDictionary *context = @{@"name" : self.context, @"date" : input};
                
                [[AIRouter sharedManager] openTimesForSeanceTableViewControllerWithContext:context];
                
                return [RACSignal empty];
            }];
}

@end
