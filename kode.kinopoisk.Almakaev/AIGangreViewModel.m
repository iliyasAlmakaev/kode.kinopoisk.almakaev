//
//  AIGangreViewModel.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 16.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIGangreViewModel.h"
#import "AIRouter.h"
#import "AIUserDefaultsManager.h"
#import "AINetManager.h"

@interface AIGangreViewModel ()

@property (strong, nonatomic) AIUserDefaultsManager *userDefaultManager;

@end

@implementation AIGangreViewModel

- (void)viewDidLoad {
    self.userDefaultManager = [[AIUserDefaultsManager alloc] init];

}

#pragma mark - Methods

- (RACSignal *)loadGanres {
    NSString *url = @"getGenres";
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
         [[AINetManager sharedManager] getData:url completition:^(id data, NSError *error) {
            if (data) {
                NSArray *genresData = [data objectForKey:@"genreData"];
                
                _genres = [MTLJSONAdapter modelsOfClass:[AIGenres class]
                                              fromJSONArray:genresData
                                                      error:nil];
                
                [subscriber sendNext:nil];
                [subscriber sendCompleted];
            } else {
                [subscriber sendError:error];
                [[AIRouter sharedManager] showGanreAlertViewError];
            }
        }];
        return nil;
    }];
}

- (RACCommand *)back {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                [[AIRouter sharedManager] closeGanreTableViewController];
                
                return [RACSignal empty];
            }];
}

- (RACCommand *)didSelectItemCommand {
    return [[RACCommand alloc]
            initWithSignalBlock:^RACSignal *(id input) {
                int i = [input intValue];
                int b = (int)_genres.count;

                if (i == b) {
                   [self.userDefaultManager setGenreID:nil setGenreName:@"любой"];
                } else {
                    _genre = _genres[i];

                    [self.userDefaultManager setGenreID:_genre.genreID setGenreName:_genre.name];
                }
                [[AIRouter sharedManager] openFilmsTableViewControllerFromGanre];
                
                return [RACSignal empty];
            }];
}

@end
