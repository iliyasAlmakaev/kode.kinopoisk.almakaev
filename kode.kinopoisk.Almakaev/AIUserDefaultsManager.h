//
//  AIUserDefaultsManager.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 28.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIUserDefaultsManager : NSObject

#pragma mark - Settings

- (void)setDefaultsSettings;

- (void)setCityID:(NSString *)cityID setCityName:(NSString *)cityName;
- (NSString *)getCityName;
- (NSString *)getCityID;

- (void)setGenreID:(NSString *)genreID setGenreName:(NSString *)genreName;
- (NSString *)getGenreID;
- (NSString *)getGenreName;

- (void)setSortingRating:(BOOL)sortingRating;
- (BOOL)getSortingRating;

#pragma mark - Film information

- (void)setFilmID:(NSString *)filmID;
- (NSString *)getFilmID;

#pragma mark - Cities

- (void)setCityData:(NSArray *)cityData;
- (NSArray *)getCityData;

#pragma mark - Dates for seance

- (void)setDatesForSeance:(NSString *)datesForSeance;
- (NSString *)getDatesForSeance;

#pragma mark - Cinema location

- (void)setCinemaName:(NSString *)cinemaName setLon:(NSString *)lon setLat:(NSString *)lat;
- (NSString *)getCinemaName;
- (NSString *)getLon;
- (NSString *)getLat;

@end
