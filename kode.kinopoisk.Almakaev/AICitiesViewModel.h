//
//  AICitiesViewModel.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 16.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIBaseViewModel.h"
#import <ReactiveObjC.h>

@interface AICitiesViewModel : AIBaseViewModel

@property (readonly, nonatomic) NSArray *cities;
@property (readonly, nonatomic) RACCommand *back;
@property (readonly, nonatomic) RACCommand *didSelectItemCommand;

- (void)viewDidLoad;

@end
