//
//  AIGanresTableViewController.m
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 27.09.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIGanresTableViewController.h"
#import "AIGanreTableViewCell.h"

static NSString *const GanreCellID = @"ganreCellID";

@interface AIGanresTableViewController ()

@property (strong, nonatomic) AIGangreViewModel *gangreViewModel;

@end

@implementation AIGanresTableViewController

- (instancetype)initWithViewModel:(AIGangreViewModel *)viewModel {
    self = [self init];
    if (self) {
        self.gangreViewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self.gangreViewModel viewDidLoad];
    [self bindUIWithViewModel];
}

#pragma mark - Methods

- (void)setupUI {
    self.navigationItem.title = @"Жанр";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"AIGanreTableViewCell" bundle:nil]
         forCellReuseIdentifier:GanreCellID];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)bindUIWithViewModel {
    self.navigationItem.leftBarButtonItem.rac_command = self.gangreViewModel.back;
    
    [[self.gangreViewModel loadGanres]
     subscribeNext:^(id x) {
         [self.tableView reloadData];
     } error:^(NSError *error) {
         NSLog(@"An error occurred: %@", error);
     }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.gangreViewModel.genres count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AIGanreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:GanreCellID];
    
    if (indexPath.row == [self.gangreViewModel.genres count]) {
        cell.textLabel.font = [UIFont boldSystemFontOfSize:18];
    
        cell.textLabel.text = @"любой";
    } else {
        self.gangreViewModel.genre = self.gangreViewModel.genres[indexPath.row];
        
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        
        cell.textLabel.text = self.gangreViewModel.genre.name;
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *i = [NSNumber numberWithInt:(int)indexPath.row];

    [self.gangreViewModel.didSelectItemCommand execute:i];
}

@end
