//
//  AIDatesForSeanceViewModel.h
//  kode.kinopoisk.Almakaev
//
//  Created by Ильяс on 14.10.16.
//  Copyright © 2016 Алмакаев Ильяс. All rights reserved.
//

#import "AIBaseViewModel.h"
#import <ReactiveObjC.h>

@interface AIDatesForSeanceViewModel : AIBaseViewModel

@property (readonly, nonatomic) NSArray *dates;
@property (readonly, nonatomic) RACSignal *loadDatesForSeance;
@property (readonly, nonatomic) RACCommand *back;
@property (readonly, nonatomic) RACCommand *openTimesForSeance;
@property (readonly, nonatomic) RACCommand *didSelectItemCommand;

- (void)viewDidLoad;

@end
